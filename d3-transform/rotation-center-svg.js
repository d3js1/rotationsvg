function MooveTriangle() {

    svg.select('#triangle')
        .transition()
        .duration(2000)
        .attr('transform', 'rotate(30)')
        .transition()
        .duration(2000)
        .attr('transform', 'rotate(0)');


}

function update(nAngle) {
    d3.select('#nAngle-value').text(nAngle);
    d3.select('#nAngle').property("value", nAngle);
    svg.select("text")
        .attr("transform", "translate(300,150) rotate("+nAngle+")");
    svg.select("polygon")
        .attr("transform", "rotate("+nAngle+")");

}

var height = 600;
var width = 600;
var base = 20;
var hauteur = 100;
var Xc = 300;
var Yc = 150;
var svg = d3.select('body')
    .append('svg')
    .attr('height', height)
    .attr('width', width);

svg
    .append('g')
    .attr('transform','translate('+Xc+','+Yc+')')
    .append('polygon')
    .attr('points', (-(base / 2)) + "," + 0 + " " + (base / 2) + "," + 0 + " " + 0 + "," + hauteur)
    .attr('style', 'fill:lime;stroke:purple;stroke-width:1')
    .attr('id', 'triangle');


svg
    .append("circle")
    .attr('r', '10')
    .attr('cx', Xc)
    .attr('cy', Yc);

svg
    .append("text")
    .style("fill", "black")
    .style("font-size", "56px")
    .attr("text-anchor", "middle")
    .text("d3noob.org");

d3.select('#nAngle')
    .on('input', function () {
        update(+this.value);
    });

update(0);

MooveTriangle();