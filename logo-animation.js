function rotate(angle) {
    d3.select("#valueAngle")
        .text(angle);

    d3.select("#pathcircleUp")
        .attr("transform", "rotate("+angle+")");

    d3.select("#pathcircleDown")
        .attr("transform", "rotate("+angle+")");

    d3.select("#pathcircleInUp")
        .attr("transform", "rotate("+angle+")");

    d3.select("#pathcircleInDown")
        .attr("transform", "rotate("+angle+")");
}

var width = 1000;
var height = 1000;

var svg = d3.select("svg")
    .attr("width", width)
    .attr("height", height);

svg.select("#main")
    .append('circle')
    .attr('r', 100)
    .attr('cy', 2900)
    .attr('cx', 2450)
    .attr('style', 'fill:red');

d3.select('#Angle')
    .on('input',function () {
        rotate(+this.value);
    });

d3.select("#main")
    .append('circle')
    .attr('style','fill:blue')
    .attr('cx',0)
    .attr('cy',0)
    .attr('r', 30);

d3.select("#circleUp")
    .append('circle')
    .attr('style','fill:blue')
    .attr('cx',0)
    .attr('cy',0)
    .attr('r', 30);

d3.select("#circleDown")
    .append('circle')
    .attr('style','fill:blue')
    .attr('cx',0)
    .attr('cy',0)
    .attr('r', 30);

d3.select("#circleInUp")
    .append('circle')
    .attr('style','fill:blue')
    .attr('cx',0)
    .attr('cy',0)
    .attr('r', 30);

d3.select("#circleInDown")
    .append('circle')
    .attr('style','fill:blue')
    .attr('cx',0)
    .attr('cy',0)
    .attr('r', 30);

