
var height = 600;
var width = 400;

var datasphere = [1,5,6,2,3,4];
var dataphere = [1,5,6,2,3,4];


var svg1 = d3.select("div.d3")
    .append("svg")
    .attr("width",width)
    .attr("height",height);

    svg1.selectAll() // créer une sectio vide qui accueille les nouveaux élement
    .data(datasphere)
    .enter()
    .append("circle")
    .attr("cy", 100)
    .attr("cx", function (d) {
        return 50 + d*50;
    })
    .attr("r", function (d) {
        return d*10;
    })
    .attr('fill', "green")
    .transition()
    .duration(2000)
    .style("fill", "gray")
    .transition()
    .duration(1000)
    .style('fill','orange');

    const margin = {top:20, right:20, bottom:100, left:120},
        widths=900 - margin.left - margin.right,
        heights=500 - margin.top - margin.bottom;

const x = d3.scaleBand()
    .range([0,widths])
    .padding(0.1);

const y = d3.scaleLinear()
    .range([heights, 0]);

const svg = d3.select("#chart").append("svg")
    .attr("id", "svg")
    .attr("width", widths + margin.left + margin.right)
    .attr("height", heights + margin.top + margin.bottom)
    .append('g')
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


const div = d3.select("body").append("div")
    .attr("class", "tooltip")
    .style("opacity", 1);

d3.tsv("data.tsv",function(error, data){

data.forEach(function(d){
d.population =+ d.population;
});

x.domain(data.map(function(d){return d.country;}));
y.domain([0,d3.max(data,function(d){return d.population;})]);

svg.append("g")
    .attr("transform", "translate(0, "+ heights +")")
    .call(d3.axisBottom(x).tickSize(0))
    .selectAll("text")
    .style("text-anchor", "end")
    .attr("dx", "-.8em")
    .attr("dy",'.15em')
    .attr("transform", "rotate(-65)");


svg.append("g")
    .call(d3.axisLeft(y).ticks(6));

svg.selectAll(".bar")
    .data(data)
    .enter().append("rect")
    .attr("class", "bar")
    .attr("x",function(d){return x(d.country);})
    .attr("width", x.bandwidth())
    .attr("y", function (d) { return y(d.population);})
    .attr("height", function (d) { return heights - y(d.population);})
    .attr("fill","rgb(0,0,200)")
    .on("mouseover", function (d) {

        div.transition()
            .duration(200)
            .style("opacity", .9);
            div.html("Population:" + d.population)
            .style("left",(d3.event.pageX) + "px")
            .style("top",(d3.event.pageY) + "px");
    })
    .on("mouseout", function (d) {
        div.transition()
            .duration(500)
            .style("opacity", 0);
    });

});
