function Histogram(DATA){
var rectwidth = 20;
var h = 200;
var w = 800;
var barpadding = 2;
var dataset = DATA;

var svg=d3.select("body")
.append('svg')
.attr('width',w)
.attr('height',h);

svg.selectAll('rect')
.data(dataset)
	.enter()
	.append('rect')
	.attr("x",function(d,i){
			return i*(w/dataset.length);
	})
.attr("y",function(d){
		return h-d; 
})
.attr("width",function(d){
		return w/dataset.length - barpadding;
			  })
.attr("height", function(d){
		  return d + "px";
				  })
.attr("fill",function(d){
		  return "rgb(0,0,"+d*10+")";
				  });
svg.selectAll('text')
.data(dataset)
	.enter()
	.append('text')
	.text(function(d){
		return Math.round(d);
	})
.attr("x",function(d,i){
		return i*(w/dataset.length);
})
.attr("y",function(d,i){
		return h-(d-10);
})
.attr("font-family", "sans-serif")
   .attr("font-size", "11px")
	       .attr("fill", "white");
}
